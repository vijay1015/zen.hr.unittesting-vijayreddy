using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace Zen.Hr.Logic.Tests
{
    [TestClass]
    public class UserServiceTests
    {
        private UserService concern;
        Mock<IUserDataAccess> userDataAccess;

        [TestMethod]
        public void Successfully_Returns_All_Users_When_Passing_In_False()
        {
            List<User> actualUsers = new List<User>();
            List<User> expectedUsers = new List<User>();

            //Getting the Mock data with all users
            expectedUsers = userDataAccess.Object.GetAllUsers();

            actualUsers = concern.GetUsers(false);
            //comparing the Actual GetUsers method result with the mocked result
            CollectionAssert.AreEqual(expectedUsers, actualUsers);
        }

        [TestMethod]
        public void Successfully_Returns_All_Active_Users_When_Passing_In_True()
        {
            List<User> actualUsers = new List<User>();
            List<User> expectedUsers = new List<User>();

            actualUsers = concern.GetUsers(true);
            //Getting the Mock data with active users
            expectedUsers = userDataAccess.Object.GetAllActiveUsers();
            //comparing the Actual GetUsers method result with the mocked result
            CollectionAssert.AreEqual(expectedUsers, actualUsers);
        }

        [TestInitialize()]
        public void Initialize()
        {
            userDataAccess = new Mock<IUserDataAccess>();

            userDataAccess.Setup(m => m.GetAllUsers())
                .Returns(
                new List<User> {
            new User{ 
                UserName = "Jhon", 
                IsActive = true
            }
            , new User{
                UserName = "Smith", 
                IsActive = false
            }
            , new User{
                UserName = "Jay", 
                IsActive = false
            }
           , new User{
                UserName = "Dan", 
                IsActive = true   
            }
        });

            userDataAccess.Setup(m => m.GetAllActiveUsers())
                .Returns(new List<User> {
            new User{
                UserName = "Jhon", 
                IsActive = true
            }
            , new User{
                UserName = "Dan", 
                IsActive = true
            }
         
        });

            concern = new UserService(userDataAccess.Object);
        }
    }
}
